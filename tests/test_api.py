import requests as r

BASE_URL = 'https://dummyjson.com'


def test_login_api_with_valid_user():
    json_data = {'username': 'kminchelle', 'password': '0lelplR'}
    headers = {'Content-Type': 'application/json'}

    response = r.post(f'{BASE_URL}/auth/login', json=json_data, headers=headers)

    assert response.status_code == 200, (
        f"Issue with {response.url} api"
        f"Next Data were used: {json_data}"
        f"Next Headers were used: {headers}"
    )
    data = response.json()
    assert 'id' in data
    assert 'username' in data
    assert 'email' in data
    assert 'firstName' in data
    assert 'lastName' in data
    assert 'gender' in data
    assert 'image' in data
    assert 'token' in data


def test_login_api_with_invalid_user():
    json_data = {'username': 'abc', 'password': '0lelplR'}
    headers = {'Content-Type': 'application/json'}

    response = r.post(f'{BASE_URL}/auth/login', json=json_data, headers=headers)

    assert response.status_code == 400, (
        f"Issue with {response.url} api"
        f"Next Data were used: {json_data}"
        f"Next Headers were used: {headers}"
    )


def test_get_posts_api(token):
    headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {token}'}

    response = r.get(f'{BASE_URL}/posts', headers=headers)

    assert response.status_code == 200, (
        f"Issue with {response.url} api"
        f"Next Headers were used: {headers}"
    )
    data = response.json()
    assert 'posts' in data
    assert 'total' in data
    assert 'skip' in data
    assert 'limit' in data


def test_get_posts_api_wrong_method(token):
    headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {token}'}

    response = r.post(f'{BASE_URL}/posts', headers=headers)

    assert response.status_code == 404, (
        f"Issue with {response.url} api"
        f"Next Headers were used: {headers}"
    )


def test_add_posts_api(token):
    json_data = {'title': 'New post', 'userId': 99}
    headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {token}'}

    response = r.post(f'{BASE_URL}/posts/add', json=json_data, headers=headers)

    assert response.status_code == 200, (
        f"Issue with {response.url} api"
        f"Next Data were used: {json_data}"
        f"Next Headers were used: {headers}"
    )
    data = response.json()
    assert data['title'] == json_data['title']
    assert data['userId'] == json_data['userId']


def test_add_posts_api_user_not_found(token):
    json_data = {'title': 'New post', 'userId': 150}
    headers = {'Content-Type': 'application/json', 'Authorization': f'Bearer {token}'}

    response = r.post(f'{BASE_URL}/posts/add', json=json_data, headers=headers)

    assert response.status_code == 404, (
        f"Issue with {response.url} api"
        f"Next Data were used: {json_data}"
        f"Next Headers were used: {headers}"
    )

