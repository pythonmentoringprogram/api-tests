import pytest
import requests as r


@pytest.fixture(scope="session")
def token():

    json_data = {'username': 'kminchelle', 'password': '0lelplR'}
    headers = {'Content-Type': 'application/json'}

    response = r.post('https://dummyjson.com/auth/login', json=json_data, headers=headers)
    return response.json()['token']
